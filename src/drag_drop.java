import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.TouchAction;
//import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;

public class drag_drop extends base {

	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		
		AndroidDriver<AndroidElement> driver = Capabilities();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		driver.findElementByXPath("//android.widget.TextView[@text='Views']").click();
		driver.findElementByXPath("//android.widget.TextView[@text='Drag and Drop']").click();
		
		WebElement start = driver.findElementById("io.appium.android.apis:id/drag_dot_1");
		WebElement end = driver.findElementById("io.appium.android.apis:id/drag_dot_3");

		@SuppressWarnings("rawtypes")
		TouchAction t = new TouchAction(driver);

		//t.longPress(longPressOptions().withDuration(ofSeconds(2)).withElement(element(start))).moveTo(element(end)).release().perform();
		//t.longPress(longPressOptions().withElement(element(start))).moveTo(element(end)).release().perform();		
		
		t.longPress(element(start)).moveTo(element(end)).release().perform();	
		
		Logger log = logging();
		
		log.trace("Trace Message!");
	    log.debug("Debug Message!");
	    log.info("Info Message!");
	    log.warn("Warn Message!");
	    log.error("Error Message!");
	    log.fatal("Fatal Message!");
	}
}
