import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;

public class base {

	public static AndroidDriver<AndroidElement> Capabilities() throws MalformedURLException {
		// TODO Auto-generated method stub
		
		File f = new File("src");
		File fs = new File(f,"ApiDemos-debug.apk");
		
		DesiredCapabilities cap = new DesiredCapabilities();
		//cap.setCapability(MobileCapabilityType.DEVICE_NAME,"Android_Test1");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME,"Pixel 2 XL API 28 Tests 1");
		
		//cap.setCapability(MobileCapabilityType.DEVICE_NAME,"Android device"); //For real devices
		cap.setCapability(MobileCapabilityType.APP, fs.getAbsolutePath());
		cap.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100000");
		
		//cap.setCapability(MobileCapabilityType.APPLICATION_NAME, "bla");

		
		AndroidDriver<AndroidElement> driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		
		return driver;
	}
	
	static final Logger LOGGER = LogManager.getLogger(base.class.getName());
	
	public static Logger logging() 
	{
		return LOGGER;
	}

}
