import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.TouchAction;
import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import static java.time.Duration.ofSeconds;

import org.openqa.selenium.WebElement;

public class swipes2 extends base {

	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		
		AndroidDriver<AndroidElement> driver = Capabilities();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		driver.findElementByXPath("//android.widget.TextView[@text='Views']").click();
		driver.findElementByXPath("//android.widget.TextView[@text='Date Widgets']").click();;
		driver.findElementByXPath("//android.widget.TextView[@text='2. Inline']").click();;
		driver.findElementByXPath("//*[@content-desc='9']").click();;
		
		//tap
		@SuppressWarnings("rawtypes")
		TouchAction t = new TouchAction(driver);
		
		WebElement we3 = driver.findElementByXPath("//*[@content-desc='15']");
		WebElement we4 = driver.findElementByXPath("//*[@content-desc='45']");

		t.longPress(longPressOptions().withElement(element(we3)).withDuration(ofSeconds(2))).moveTo(element(we4)).release().perform();
	}

}
