import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.TouchAction;
import static io.appium.java_client.touch.TapOptions.tapOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

import org.openqa.selenium.WebElement;

public class swipes extends base {

	public static void main(String[] args) throws MalformedURLException {
		// TODO Auto-generated method stub
		
		AndroidDriver<AndroidElement> driver = Capabilities();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		driver.findElementByXPath("//android.widget.TextView[@text='Views']").click();
		
		//tap
		@SuppressWarnings("rawtypes")
		TouchAction t = new TouchAction(driver);
		
		WebElement we = driver.findElementByXPath("//android.widget.TextView[@text='Date Widgets']");
		t.tap(tapOptions().withElement(element(we))).perform();
		
		WebElement we2 = driver.findElementByXPath("//android.widget.TextView[@text='2. Inline']");
		t.tap(tapOptions().withElement(element(we2))).perform();
		
		WebElement we3 = driver.findElementByXPath("//*[@content-desc='10']");
		we3.click();
		
		driver.findElementByXPath("//*[@content-desc='30']").click();;
	}

}
